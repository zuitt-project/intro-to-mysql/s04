
--Mini-activities:
--Add the following records in artists table:

--Name
----
--Taylor Swift
--Lady Gaga
--Justin Bieber
--Ariana Grande
--Bruno Mars


-- Add the following records in albums table:

-- Name Year Artist
-- --------------------------------------
-- Fearless 2008 Taylor Swift
-- Red 2012 Taylor Swift
-- A Star Is Born 2018 Lady Gaga
-- Born This Way 2011 Lady Gaga
-- Purpose 2015 Justin Bieber
-- Believe 2012 Justin Bieber
-- Dangerous Woman 2016 Ariana Grande
-- Thank U, Next 2019 Ariana Grande
-- 24K Magic 2016 Bruno Mars
-- Earth to Mars 2011 Bruno Mars

USE music_db;

INSERT INTO artists (name)
VALUES ("Taylor Swift"),
	   ("Lady Gaga"),
	   ("Justin Bieber"),
	   ("Ariana Grande"),
	   ("Bruno Mars");

INSERT INTO albums (name, year, artist_id)
VALUES ("Fearless", "2008-01-01", 3),
	   ("Red", "2012-01-01", 3),  
	   ("A Star Is Born", "2018-01-01", 4),  
	   ("Born This Way", "2011-01-01", 4),  
	   ("Purpose", "2015-01-01", 5),  
	   ("Believe", "2012-01-01", 5),  
	   ("Dangerous Woman", "2016-01-01", 6),  
	   ("Thank U, Next", "2019-01-01", 6),  
	   ("24K Magic", "2016-01-01", 7),  
	   ("Earth to Mars", "2011-01-01", 7);  

INSERT INTO songs (title, length, genre, album_id)
VALUES ("Fearless", "246", "Pop Rock", 6),
 	   ("Lovestory", "213", "Country Rock", 6),
 	   ("State of Grace", "313", "Alternative Rock", 7),
	   ("Red", "204", "Country", 7),
	   ("Black Eyes", "181", "Rock n Roll", 8),
	   ("Shallow", "201", "Country, Rock, Folk Rock", 8),
	   ("Sorry", "192", "Dancehall-poptropical housemoombahton", 10),
	   ("Boyfriend", "251", "Pop", 11),
	   ("Into You", "242", "EDM House", 12 ),
	   ("Thank U, Next", "196", "Pop, R&B",13 ),
	   ("24K Magic", "207", "Funk, Disco, R&B", 14),
	   ("Lost", "192", "Pop",15 );
	 
 
 --Retrive records from songs table where ID is equal to 1 or 3 or 5.

 SELECT *
 FROM songs
 WHERE id = 1
 OR id = 3
 OR id = 5;

 SELECT *
 FROM songs
 WHERE id IN (1, 3, 5);

 --Combining conditions using AND

 --Retrieve records from SONGS where album_id is equal 4 and id is less than 8.

 SELECT * 
 FROM songs 
 WHERE album_id = 4 
 AND id <=8;

 --Find using partial matches

 --The keyword is found at the end
 	--Retrieve records from the songs table where the title ends in 'y'.

 	SELECT *
 	FROM songs
 	WHERE title LIKE "%y";

 --The keyword is found from the start
 	--Retrieve records from the songs table where the title starts in 's'.

 	SELECT *
 	FROM songs
 	WHERE title LIKE "s%";

 --The keyword is found in between
 	--Retrieve records from the songs table where the title  'u'.

 	SELECT *
 	FROM songs
 	WHERE title LIKE "%u%";

 	---
 	SELECT *
 	FROM songs
 	WHERE title LIKE "%TO%";


 --We know some keyword

 --Get specific pattern
 	--Retrieve the records from albums table where year released was in the 1990's.

 	SELECT *
 	FROM albums
 	WHERE year LIKE "199_%";

-- Retrieve the records from albums table where year released was in the 2000's.
	
	SELECT *
 	FROM albums
 	WHERE year LIKE "20_2%";

--Sorting records: ascending (ASC) or descending (DESC)

	SELECT title
 	FROM songs
 	ORDER BY title ASC;

 	SELECT title
 	FROM songs
 	ORDER BY title DESC;

 --Retrieve distinct records

 	SELECT DISTINCT genre FROM songs;


 	SELECT DISTINCT genre FROM songs ORDER BY genre ASC;

--Joining tables

-- Retrieve all records from the artists and albums tables.
	
	SELECT *
	FROM artists JOIN albums
	ON artists.id = albums.artist_id;

-- 

	SELECT *
	FROM artists 
	JOIN albums	ON artists.id = albums.artist_id
	JOIN songs	ON albums.id = songs.album_id;

-- 

	SELECT artists.name, albums.name
	FROM artists JOIN albums
	ON artists.id = albums.artist_id;

-- 

	SELECT artists.name AS artist_name, albums.name AS album_name
	FROM artists JOIN albums
	ON artists.id = albums.artist_id;

-- 

	SELECT artists.name AS artist_name, albums.name AS album_name
	FROM artists AS ar JOIN albums AS al
	ON artists.id = albums.artist_id;
	