-- Find all artist that has letter D in its name

	SELECT *
 	FROM artists
 	WHERE name LIKE "%D%";

-- Find all songs that has a length of less than 230
	
	SELECT *
 	FROM songs
 	WHERE length > "230"; 

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
	
	SELECT albums.name AS album_name, songs.name AS song_name, songs.length AS songs_length
	FROM albums JOIN songs
	ON albums.id = songs.album_id;


-- Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)

	SELECT *
	FROM albums JOIN songs
	ON albums.id = songs.album_id
	WHERE albums.name LIKE "%A%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)
	
	SELECT name
 	FROM albums
 	ORDER BY name DESC
 	LIMIT 4;
	
-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)

	SELECT *
	FROM albums JOIN songs
	ON albums.id = songs.album_id
	ORDER BY albums.name DESC,
			 songs.title ASC;

	
	